<?php
require_once("class.php");
// カレントの言語を日本語に設定する
mb_language("Japanese");
// 内部文字エンコードを設定する
mb_internal_encoding("shift_jis");
// 日時を取得
$y = date("Y");
$m = date("m");
$d = date("d");
$h = date("H");
$i = date("i");
$s = date("s");
//　エラー
$err = "";
// 初期化
$news = "";
//-------------------------------------------------------
//　POSTされたとき
//-------------------------------------------------------
if($_SERVER['REQUEST_METHOD']=="POST"){
if(isset($_POST["submit"])){
	$err = "";
	//-----------------------------------------------
	//　POSTされたデータを取得
	//-----------------------------------------------
	//　新規追加
	$news = htmlspecialchars($_POST["news"],ENT_QUOTES);
	$news = mb_convert_encoding($news,"SJIS","ASCII,JIS,UTF-8,EUC-JP,SJIS");
	
	//-----------------------------------------------
	//　チェック
	//-----------------------------------------------
	$check = new upcheck();
	//　news
	if($news == ""){
		$err = "<br /><div class=\"fol\">記事を記入してください。</div>";
	}
	if($err == ""){
		//------------------------------------------------------------------------------------------------------
		//　アップロード
		//------------------------------------------------------------------------------------------------------
		$dirpass = getcwd();
		$dirnm = $y.$m.$d.$h.$i.$s;
		$dir = $dirpass."/news";
		if(file_exists($dir)==false){
			mkdir($dir, 0777);
			chmod ($dir, 0777);
		}
		//　テキスト
		$txt .= $y."-".$m."-".$d."\n";
		$txt .= $news;
		if ( get_magic_quotes_gpc( ) ) {
		    $txt = stripslashes($txt);
		}
		
		// テキスト書き込み
		$filenm = $y.$m.$d.$h.$i.$s;
		$txtnm = $dir."/".$filenm.".cgi";
		$fp = @fopen($txtnm,"a");
		if(!$fp){
			exit ("ファイル書き込みのオープンに失敗");
		}else{
			flock($fp,LOCK_EX);
			fwrite($fp,$txt);
			flock($fp,LOCK_UN);
			fclose($fp);
			chmod($txtnm, 0777);
		}
		header("Location: news.php");
		exit;
	}
}else{
	foreach($_POST as $key => $value){
		if(preg_match("/[0-9]{13}/",$key)){
			$delfile = $key.".cgi";
		}
	}
	$pass = "./news/".$delfile;
	$dir = "./news/";
	if(file_exists($pass) === true){
		$od = opendir($dir);
		unlink($pass);
		closedir($od);
	}
}
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=shift-jis" />
<title>News & Topics</title>
<link rel=stylesheet href="css/adm.css" type="text/css">
</head>

<body>
<div id="container">

<!--header-->
<div id="header">
<div id="box">
	<div class="hdmn01"><img src="image/admin_hdbtn01.gif" /></div>
	<div class="hdmn02"></div>
</div>
</div>

<div><img src="image/he.png" /></div>

<!--contents-->
<div id="content">

<!--新規-->
<div class="pb20"><img src="image/admin_ttl01.gif" /></div>
<!--form-->
<form action="<?=$_SERVER['PHP_SELF']?>" method="post" enctype="multipart/form-data">
	<div><img src="image/contact_form01.gif" /></div>
	<div id="formbox">

		<div id="box">
			<div class="formttl">最新記事</div>
			<div class="forminp"><textarea name="news" cols="85" rows="5" style="font-size:12px"><?=$news?></textarea>
			<?=$err?></div>
			<div class="formbtn"><input type="submit" name="submit" value="登 録" /></div>
		</div>

	</div>
	<div class="pb30"><img src="image/contact_form03.gif" /></div>

<!--確認・削除-->
<div class="pb20"><img src="image/admin_ttl02.gif" /></div>
<?php
//内容確認
$dir = "./news/";
$files = array();
//ディレクトリを開いて変数に代入
$od = opendir($dir);
while(false !== ($list = readdir($od))){
	if(preg_match("/[0-9]{13}/",$list)){
		array_push($files, $list);
	}
}
closedir($od);
rsort($files);
//　更新情報ファイルを開いてテーブルタグ追加表示
foreach($files as $one){		//一件づつ【onelist】に送る
	echo onelist($one);
}
//　テーブルタグで表示
function onelist($one){
	$check = new upcheck();
	$tags = "";
	$pass = "./news/".$one;
	list($id,$gomi) = explode(".", $one);
	$fo = fopen($pass,"r");
	while(!feof($fo)){
		$data[] = fgets($fo);
	}
	fclose($fo);
	$tags .= "<div id=\"box\">\n";
	$tags .= "	<div class=\"formdelttl\">";
	foreach($data as $key => $value){
		if($key>=1){
			$value = $check->conv($value);
			$tags.= nl2br($value);
		}
	}
	$tags .= "</div>\n";
	$tags .= "	<div class=\"formbtn\">";
	$tags .= "<input type=\"submit\" name=\"{$id}\" value=\"削 除\" />";
	$tags .= "	</div>\n";
	$tags .= "</div>\n";
	$tags .= "<div class=\"pb5\"><img src=\"image/admin_line.gif\" /></div>\n";
	return $tags;
}
?>

</form>

</div>
<!--footer-->
<div id="ftmn">新着情報</div>
<div class="pb20"><img src="image/foot.png" /></div>

</div>
</body>
</html>
