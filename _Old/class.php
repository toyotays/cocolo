<?php
//============================================
//	チェック用クラス
//============================================
class upcheck{
	//----------------------------------------
	//　変数の宣言
	//----------------------------------------
	var $str = "";
	var $nameerr = "";
	var $mailerr = "";
	var $msgerr = "";
	function upcheck(){}
	
	function conv($str){
		$this->str = mb_convert_encoding($str,"SJIS","ASCII,JIS,UTF-8,EUC-JP,SJIS");
		return $this->str;
	}
	
	function chname($name){
		if(strlen($name)==0){
			$this->nameerr = "お名前が未記入です";
		}
		return $this->nameerr;
	}

	function chhuri($huri){
		if(strlen($huri)==0){
			$this->hurierr = "お名前（フリガナ）が未記入です";
		}else{
			if(!preg_match("/^(\x83[\x40-\x96]|\x81[\x52\x53\x5b])+$/",$huri)){
				$this->hurierr = "お名前（フリガナ）は全角カタカナでご記入ください";
			}
		}
		return $this->hurierr;
	}
	
	function chmail($mail){
		if(strlen($mail)>0){
			$check = "/([a-zA-Z0-9_\.-]+\@)([a-zA-Z0-9_\.-]+)([a-zA-Z]+)/";
			if(!preg_match($check,$mail)){
				$this->mailerr = "メールアドレスの記述に誤りがあります";
			}
		}else{
			$this->mailerr = "メールアドレスを記入してください";
		}
		return $this->mailerr;
	}
	
	function chmsg($msg){
		if(strlen($msg)==0){
			$this->msgerr = "内容をご記入ください";
		}
		return $this->msgerr;
	}
	

}
//============================================
//	リサイズ用クラス
//============================================
class Image {
    
    var $file;
    var $image_width;
    var $image_height;
    var $width;
    var $height;
    var $ext;
    var $types = array('','gif','jpeg','png','swf');
    var $quality = 80;
    var $top = 0;
    var $left = 0;
    var $crop = false;
    var $type;
    
    function Image($name='') {
        $this->file = $name;
        $info = getimagesize($name);
        $this->image_width = $info[0];
        $this->image_height = $info[1];
        $this->type = $this->types[$info[2]];
        $info = pathinfo($name);
        $this->dir = $info['dirname'];
        $this->name = str_replace('.'.$info['extension'], '', $info['basename']);
        $this->ext = $info['extension'];
    }
    
    function dir($dir='') {
        if(!$dir) return $this->dir;
        $this->dir = $dir;
    }
    
    function name($name='') {
        if(!$name) return $this->name;
        $this->name = $name;
    }
    
    function width($width='') {
        $this->width = $width;
    }
    
    function height($height='') {
        $this->height = $height;
    }
    
    function resize($percentage=50) {
        if($this->crop) {
            $this->crop = false;
            $this->width = round($this->width*($percentage/100));
            $this->height = round($this->height*($percentage/100));
            $this->image_width = round($this->width/($percentage/100));
            $this->image_height = round($this->height/($percentage/100));
        } else {
            $this->width = round($this->image_width*($percentage/100));
            $this->height = round($this->image_height*($percentage/100));
        }
        
    }
    
    function crop($top=0, $left=0) {
        $this->crop = true;
        $this->top = $top;
        $this->left = $left;
    }
    
    function quality($quality=80) {
        $this->quality = $quality;
    }
    
    function show() {
        $this->save(true);
    }
    
    function save($show=false) {

        if($show) @header('Content-Type: image/'.$this->type);
        
        if(!$this->width && !$this->height) {
            $this->width = $this->image_width;
            $this->height = $this->image_height;
        } elseif (is_numeric($this->width) && empty($this->height)) {
            $this->height = round($this->width/($this->image_width/$this->image_height));
        } elseif (is_numeric($this->height) && empty($this->width)) {
            $this->width = round($this->height/($this->image_height/$this->image_width));
        } else {
            if($this->width<=$this->height) {
                $height = round($this->width/($this->image_width/$this->image_height));
                if($height!=$this->height) {
                    $percentage = ($this->image_height*100)/$height;
                    $this->image_height = round($this->height*($percentage/100));
                }
            } else {
                $width = round($this->height/($this->image_height/$this->image_width));
                if($width!=$this->width) {
                    $percentage = ($this->image_width*100)/$width;
                    $this->image_width = round($this->width*($percentage/100));
                }
            }
        }
        
        if($this->crop) {
            $this->image_width = $this->width;
            $this->image_height = $this->height;
        }

        if($this->type=='jpeg') $image = imagecreatefromjpeg($this->file);
        if($this->type=='png') $image = imagecreatefrompng($this->file);
        if($this->type=='gif') $image = imagecreatefromgif($this->file);
        
        $new_image = imagecreatetruecolor($this->width, $this->height);
        imagecopyresampled($new_image, $image, 0, 0, $this->top, $this->left, $this->width, $this->height, $this->image_width, $this->image_height);
        
        $name = $show ? null: $this->dir.DIRECTORY_SEPARATOR.$this->name.'.'.$this->ext;
        if($this->type=='jpeg') imagejpeg($new_image, $name, $this->quality);
        if($this->type=='png') imagepng($new_image, $name);
        if($this->type=='gif') imagegif($new_image, $name);
        
        imagedestroy($image); 
        imagedestroy($new_image);
        
    }
    
}

$atesaki = "tetsuya-yamaoka@y3.dion.ne.jp";
?>