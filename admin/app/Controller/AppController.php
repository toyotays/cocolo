<?php
/*
	共通コントローラ
	2014/12/30 nakayama
*/
App::uses('Controller', 'Controller');


class AppController extends Controller
{
	public $uses = array
	(
		'Admin',
		'Article'
	);
	
	
	public $components = array
	(
		'Session',
		'Auth' => array
		(
			'authenticate' => array
			(
				'Form' => array
				(
					'userModel' => 'Admin',
					'fields' => array
					(
						'username' => 'username',
						'password' => 'password'
					)
				)
			),
			'loginAction' => array
			(
				'controller' => 'auth',
				'action' => 'login'
			)
		)
	);
	
	
	
	/*
		処理前フィルタ
	*/
	public function beforeFilter()
	{
		/*
			ユーザ情報取得
		*/
		$this->authUser = $this->Auth->user();
	}
	
	
	
	public function d($data, $title = FALSE)
	{
		echo '<hr>';
		
		
		if ($title)
		{
			printf
			(
				'<h1>%s</h1>',
				$title
			);
		}
		
		
		printf
		(
			'<pre>%s</pre>',
			print_r($data, TRUE)
		);
	}
}
