<?php
/*
	記事コントローラ
	2014/12/30 nakayama
*/
App::uses('AppController', 'Controller');

class ArticleController extends AppController
{
	/*
		ページング
	*/
	var $paginate = array
	(
		'limit' => 10,
		'conditions' => array
		(
			'Article.del_flg' => 0
		),
		'order' => array
		(
			'Article.article_id' => 'desc'
		)
	);
	
	
	/*
		記事一覧
	*/
	public function index()
	{
		$data = $this->paginate('Article');
		
		$this->set('data', $data);
	}
	
	
	
	/*
		記事作成
	*/
	public function regist()
	{
		if (!$this->request->is('post'))
		{
			return;
		}
		
		
		/*
			入力チェック
		*/
		$this->Article->create();
		$this->Article->set($this->data);
		$isValid = $this->Article->validates($this->data);
		
		
		$this->set('validationErrors', $this->Article->validationErrors);
		
		
		if (!$isValid)
		{
			return;
		}
		
		
		/*
			画面表示判定
		*/
		if (isset($this->data['submit-back']))
		{
			/*
				入力画面
			*/
			return;
		}
		
		
		if (!isset($this->data['submit-ok']))
		{
			/*
				確認画面
			*/
			$this->render('regist_confirm');
			
			return;
		}
		
		
		/*
			記事作成
		*/
		$this->Article->save(FALSE, FALSE);
		
		
		$this->Session->setFlash('記事を作成しました。');
		$this->set('flashClass', 'alert-success');
		
		$this->redirect
		(
			array
			(
				'action' => 'index'
			)
		);
	}
	
	
	
	/*
		記事編集
	*/
	public function edit()
	{
		if (!isset($this->params['named']['article_id']))
		{
			throw new Exception('記事番号が指定されていない');
			return;
		}
		
		
		$this->Article->create();
		$this->Article->id = $this->params['named']['article_id'];
		
		
		/*
			編集画面
		*/
		if (!$this->request->is('post'))
		{
			/*
				フォームセット
			*/
			$this->request->data = $this->Article->read();
			
			$this->render('regist');
			
			return;
		}
		
		
		/*
			入力チェック
		*/
		$this->Article->set($this->data);
		$isValid = $this->Article->validates($this->data);
		
		
		$this->set('validationErrors', $this->Article->validationErrors);
		
		
		if (!$isValid)
		{
			$this->render('regist');
			
			return;
		}
		
		
		/*
			画面表示判定
		*/
		if (isset($this->data['submit-back']))
		{
			/*
				入力画面
			*/
			$this->render('regist');
			
			return;
		}
		
		
		if (!isset($this->data['submit-ok']))
		{
			/*
				確認画面
			*/
			$this->render('regist_confirm');
			
			return;
		}
		
		
		/*
			更新
		*/
		$this->Article->save(FALSE, FALSE);
		
		
		$this->Session->setFlash('記事を反映しました。');
		$this->set('flashClass', 'alert-success');
		
		$this->redirect
		(
			array
			(
				'action' => 'index'
			)
		);
	}
}