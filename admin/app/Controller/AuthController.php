<?php
/*
	認証コントローラ
	2014/12/30 nakayama
*/
App::uses('AppController', 'Controller');


class AuthController extends AppController
{
	/*
		処理前フィルタ
	*/
	public function beforeFilter()
	{
		parent::beforeFilter();
		
		
		/*
			認証許可アクション
		*/
		$this->Auth->allow('login');
	}
	
	
	
	/*
		ログイン
	*/
	public function login()
	{
		/*
			レイアウトなし
		*/
		$this->layout = FALSE;
		
		
		if (!$this->request->is('post'))
		{
			return;
		}
		
		
		
		/*
			管理者レコードが存在するか確認
		*/
		$count = $this->Admin->find('count');
		
		
		if ($count == 0)
		{
			/*
				デフォルト管理者を作成
			*/
			$this->Admin->create();
			
			$this->Admin->set
			(
				array
				(
					'admin_id' => 1,
					'username' => 'admin1',
					'password' => AuthComponent::password('adminkey1')
				)
			);
			
			$this->Admin->save(FALSE, FALSE);
		}
		
		
		
		/*
			ログイン判定
		*/
		if ($this->Auth->login())
		{
			return $this->redirect($this->Auth->redirect());
		}
		else
		{
			$this->Session->setFlash('パスワードが違います。');
			$this->set('flashClass', 'alert-danger');
		}
	}
	
	
	
	/*
		ログアウト
	*/
	public function logout()
	{
		/*
			レイアウトなし
		*/
		$this->layout = FALSE;
		
		
		$this->Auth->logout();
		$this->Session->destroy();
		
		
		$this->Session->setFlash('ログアウトしました。');
		$this->set('flashClass', 'alert-success');
		
		
		$this->render('login');
	}
	
	
	
	/*
		パスワード
	*/
	public function password()
	{
		if (!$this->request->is('post'))
		{
			return;
		}
		
		
		/*
			現在のパスワードが正しいか判定
		*/
		$isCheckPassword = $this->Admin->checkPassword
		(
			$this->authUser['admin_id'],
			$this->data['Admin']['password_old']
		);
		
		
		if (!$isCheckPassword)
		{
			$this->set
			(
				'validationErrors',
				array
				(
					'password_old' => array('正しく入力してください。')
				)
			);
			
			return;
		}
		
		
		/*
			新しいパスワードの入力チェック
		*/
		$this->Admin->create();
		$this->Admin->set($this->data);
		$isValid = $this->Admin->validates($this->data);
		
		
		$this->set('validationErrors', $this->Admin->validationErrors);
		
		
		if (!$isValid)
		{
			return;
		}
		
		
		/*
			パスワード変更
		*/
		$this->Admin->create();
		
		$this->Admin->set
		(
			array
			(
				'admin_id' => $this->authUser['admin_id'],
				'password' => AuthComponent::password($this->data['Admin']['password'])
			)
		);
		
		$this->Admin->save(FALSE, FALSE);
		
		
		$this->Session->setFlash('パスワードを変更しました。');
		$this->set('flashClass', 'alert-success');
	}
}