<?php
/*
	出力コントローラ
	2015/01/03 nakayama
*/
App::uses('AppController', 'Controller');


class OutputController extends AppController
{
	/*
		処理前フィルタ
	*/
	public function beforeFilter()
	{
		parent::beforeFilter();
		
		/*
			レイアウト指定
		*/
		$this->layout = 'output';
		
		
		/*
			すべてのアクションを許可
		*/
		$this->Auth->allow();
		
		
		/*
			出力タイプ
		*/
		$this->response->type
		(
			array
			(
				'js' => 'application/x-javascript'
			)
		);
		
		$this->response->type('js');
	}
	
	
	
	/*
		記事一覧
	*/
	public function articles()
	{
		/*
			取得設定
		*/
		$options = array
		(
			'conditions' => array
			(
				'is_publish' => 1
			),
			'order' => array
			(
				'article_id' => 'desc'
			)
		);
		
		
		/*
			表示件数
		*/
		if
		(
			isset($this->params['named']['limit']) &&
			is_numeric($this->params['named']['limit'])
		)
		{
			$options['limit'] = $this->params['named']['limit'];
		}
		
		
		/*
			取得
		*/
		$data = $this->Article->find
		(
			'all',
			$options
		);
		
		
		$this->set('data', $data);
	}
	
	
	
	/*
		記事詳細
	*/
	public function detail()
	{
		if (!isset($this->params['named']['article_id']))
		{
			throw new Exception('記事番号が指定されていない');
			return;
		}
		
		
		$data = $this->Article->find
		(
			'first',
			array
			(
				'conditions' => array
				(
					'article_id' => $this->params['named']['article_id'],
					'is_publish' => 1
				)
			)
		);
		
		
		if (!$data)
		{
			throw new Exception('指定された記事は存在しない');
			return;
		}
		
		
		$this->set('data', $data);
	}
}