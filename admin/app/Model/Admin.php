<?php
/*
	ユーザモデル
	2014/12/30 nakayama
*/
App::uses('AppModel', 'Model');


class Admin extends AppModel
{
	/*
		テーブル名
	*/
	public $useTable = 'cms_admin';
	
	
	/*
		主キー
	*/
	public $primaryKey = 'admin_id';
	
	
	/*
		バリデーション
	*/
	public $validate = array
	(
		'password' => array
		(
			'notEmpty' => array
			(
				'rule' => 'notEmpty',
				'message' => '入力してください。'
			),
			'custom' => array
			(
				'rule' => array
				(
					'custom',
					'/^[0-9a-z]+$/'
				),
				'message' => '半角小文字英数字で入力してください。'
			),
			'between' => array
			(
				'rule' => array
				(
					'between',
					5,
					20
				),
				'message' => '5～20文字で入力してください。'
			),
		)
	);
	
	
	
	/*
		パスワード確認
		adminId: 管理者番号
		password: パスワード
		戻り値: 正しい
	*/
	public function checkPassword($adminId, $password)
	{
		$data = $this->find
		(
			'first',
			array
			(
				'conditions' => array
				(
					'admin_id' => $adminId,
					'password' => AuthComponent::password($password)
				)
			)
		);
		
		
		if (!$data)
		{
			return FALSE;
		}
		
		return TRUE;
	}
}
