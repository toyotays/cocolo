<?php
/*
	共通モデル
	2015/01/01 nakayama
*/
class AppModel extends Model
{
	public function d($data, $title = FALSE)
	{
		echo '<hr>';
		
		
		if ($title)
		{
			printf
			(
				'<h1>%s</h1>',
				$title
			);
		}
		
		
		printf
		(
			'<pre>%s</pre>',
			print_r($data, TRUE)
		);
	}
}
