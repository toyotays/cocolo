<?php
/*
	記事モデル
	2015/01/02 nakayama
*/
class Article extends AppModel
{
	/*
		テーブル名
	*/
	public $useTable = 'cms_article';
	
	
	/*
		主キー
	*/
	public $primaryKey = 'article_id';
	
	
	/*
		バリデーション
	*/
	public $validate = array
	(
		'title' => array
		(
			'notEmpty' => array
			(
				'rule' => 'notEmpty',
				'message' => '入力してください。'
			),
			'between' => array
			(
				'rule' => array
				(
					'between',
					1,
					100
				),
				'message' => '1～100文字で入力してください。'
			)
		),
		'body' => array
		(
			'notEmpty' => array
			(
				'rule' => 'notEmpty',
				'message' => '入力してください。'
			),
			'between' => array
			(
				'rule' => array
				(
					'maxLength',
					500000
				),
				'message' => '500,000文字以内で入力してください。'
			)
		)
	);
	
	
	/*
		保存前
	*/
	public function beforeSave($options = array())
	{
		/*
			主キー番号が含まれているか判定
		*/
		if (empty($this->id))
		{
			/*
				作成日時を指定
			*/
			$this->data[$this->alias]['regist_dt'] = time();
		}
		
		
		/*
			更新日時を指定
		*/
		$this->data[$this->alias]['update_dt'] = time();
		
		
		return TRUE;
	}
}
