<div class="content-box-large">
 	<?php
 		echo $this->Html->link
 		(
 			'記事作成',
 			array
 			(
 				'controller' => 'article',
 				'action' => 'regist'
 			),
 			array
 			(
 				'class' => 'btn btn-primary'
 			)
 		);
 	?>
</div>


<div class="content-box-large">
	<div class="panel-heading">
		<div class="panel-title">
			記事一覧
		</div>
	</div>
	
	<div class="panel-body">
		<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="example">
			<thead>
				<tr>
					<th class="center">記事番号</th>
					<th class="center">公開</th>
					<th>タイトル</th>
					<th>本文</th>
					<th class="center">更新日時</th>
				</tr>
			</thead>
			<tbody>
<?php
	foreach ($data as $row)
	{
?>
				<tr>
					<td class="center" >
						<?php
							echo htmlspecialchars($row['Article']['article_id']);
						?>
					</td>
					
					<td class="center">
						<?php
							if ($row['Article']['is_publish'])
							{
								echo '公開';
							}
							else
							{
								echo '非公開';
							}
						?>
					</td>
					<td>
						<?php
							echo $this->Html->link
							(
								$row['Article']['title'],
								array
								(
									'action' => 'edit',
									'article_id' => $row['Article']['article_id']
								)
							);
						?>
					</td>
					<td>
						<?php
							echo htmlspecialchars
							(
								mb_substr
								(
									strip_tags($row['Article']['body']),
									0,
									15
								).'...'
							);
						?>
					</td>
					<td class="center">
						<?php
							echo htmlspecialchars
							(
								date('Y/m/d H:i', $row['Article']['update_dt'])
							);
						?>
					</td>
				</tr>
<?php
	}
?>
			</tbody>
		</table>



        <div class="row">
        	<div class="col-xs-12">
				<?php
					echo $this->element('paging');
				?>
            </div>
        </div>


	</div>
</div>