<script>
	// 公開削除自動切換
	$
	(
		function()
		{
			// 削除が選択されたとき非公開にする
			$('#ArticleDelFlg').change
			(
				function()
				{
					if ($(this).val() == 1)
					{
						$('#ArticleIsPublish').val(0);
					}
				}
			);
			
			
			// 公開が選択されたとき削除する
			$('#ArticleIsPublish').change
			(
				function()
				{
					if ($(this).val() == 1)
					{
						$('#ArticleDelFlg').val(0);
					}
				}
			);
		}
	);
</script>



<div class="content-box-large">
	<div class="panel-heading">
		<div class="panel-title">
			<?php
				if ($this->request->action == 'regist')
				{
					echo '記事作成';
				}
				else
				{
					echo '記事編集';
				}
			?>
        </div>
	</div>
	
	<div class="panel-body">
		
    	<?php
    		echo $this->Form->create
    		(
    			'Article',
    			array
    			(
    				'type' => 'post',
        			'class' => 'form-horizontal',
        			'role' => 'form'
    			)
    		);
    	?>
    	
            <fieldset>
            	
                <?php
                	$errorClass = '';
                	$errorMessage = '';
                	
                	
                	if (isset($validationErrors['title']))
                	{
                		$errorClass = ' has-error';
                		$errorMessage = implode('', $validationErrors['title']);
                	}
                ?>
            	
                <div class="form-group <?php echo $errorClass; ?>">
                    <label class="col-sm-2 control-label">
                    	タイトル
                    </label>
                    <div class="col-sm-10">
                        <?php
                        	echo $this->Form->input
                        	(
                        		'title',
                        		array
                        		(
                        			'type' => 'text',
                        			'class' => 'form-control',
                        			'div' => FALSE,
                        			'label' => FALSE
                        		)
                        	);
                        	
                        	
                        	if (!empty($errorMessage))
                        	{
                        		printf
                        		(
                        			'<span class="help-block"><i class="fa fa-warning"></i> %s</span>',
                        			$errorMessage
                        		);
                        	}
                        ?>
                    </div>
                </div>
                
                <?php
                	$errorClass = '';
                	$errorMessage = '';
                	
                	
                	if (isset($validationErrors['body']))
                	{
                		$errorClass = ' has-error';
                		$errorMessage = implode('', $validationErrors['body']);
                	}
                ?>
                
                <div class="form-group <?php echo $errorClass; ?>">
                    <label class="col-sm-2 control-label">
                    	本文
                    </label>
                    <div class="col-sm-10">
                        <?php
                        	echo $this->Form->textarea
                        	(
                        		'body',
                        		array
                        		(
                        			'type' => 'text',
                        			'class' => 'form-control tinymce',
                        			'required' => FALSE,
                        			'div' => FALSE,
                        			'label' => FALSE
                        		)
                        	);
                        	
                        	
                        	if (!empty($errorMessage))
                        	{
                        		printf
                        		(
                        			'<span class="help-block"><i class="fa fa-warning"></i> %s</span>',
                        			$errorMessage
                        		);
                        	}
                        ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">
                    	公開
                    </label>
                    <div class="col-sm-10">
                        <?php
                        	echo $this->Form->select
                        	(
                        		'is_publish',
                        		array
                        		(
                        			1 => '公開する',
                        			0 => '公開しない'
                        		),
                        		array
                        		(
                        			'class' => 'form-control',
                        			'empty' => FALSE,
                        			'div' => FALSE,
                        			'label' => FALSE
                        		)
                        	);
                        ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">
                    	削除
                    </label>
                    <div class="col-sm-10">
                        <?php
                        	echo $this->Form->select
                        	(
                        		'del_flg',
                        		array
                        		(
                        			0 => '削除しない',
                        			1 => '削除する',
                        		),
                        		array
                        		(
                        			'class' => 'form-control',
                        			'empty' => FALSE,
                        			'div' => FALSE,
                        			'label' => FALSE
                        		)
                        	);
                        ?>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                    	<?php
							echo $this->Form->submit
							(
								'確認',
								array
								(
									'class' => 'btn btn-primary',
									'div' => FALSE
								)
							);
                    	?>
                    </div>
                </div>
            </fieldset>
    	<?php
    		echo $this->Form->end();
    	?>
    </div>
</div>



