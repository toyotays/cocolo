<div class="content-box-large">
	<div class="panel-heading">
		<div class="panel-title">
			<?php
				if ($this->request->action == 'regist')
				{
					echo '記事作成';
				}
				else
				{
					echo '記事編集';
				}
			?>
        </div>
	</div>
	
	<div class="panel-body">
		
    	<?php
    		echo $this->Form->create
    		(
    			'Article',
    			array
    			(
    				'type' => 'post',
        			'class' => 'form-horizontal',
        			'role' => 'form'
    			)
    		);
    	?>
    		
    		
    		<p>
    			この内容で反映するには確定ボタンを押してください。<br><br>
    		</p>
    		
    		
            <fieldset>
                <div class="form-group">
                    <label class="col-sm-2">
                    	タイトル
                    </label>
                    <div class="col-sm-10">
                        <?php
                        	echo htmlspecialchars($this->request->data['Article']['title']);
                        	echo $this->Form->hidden('title');
                        ?>
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="col-sm-2">
                    	本文
                    </label>
                    <div class="col-sm-10">
                        <?php
                        	echo $this->request->data['Article']['body'];
                        	echo $this->Form->hidden('body');
                        ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2">
                    	公開
                    </label>
                    <div class="col-sm-10">
                        <?php
                        	if ($this->request->data['Article']['is_publish'])
                        	{
                        		echo 'する';
                        	}
                        	else
                        	{
                        		echo 'しない';
                        	}
                        	
                        	echo $this->Form->hidden('is_publish');
                        ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2">
                    	削除
                    </label>
                    <div class="col-sm-10">
                        <?php
                        	if ($this->request->data['Article']['del_flg'])
                        	{
                        		echo 'する　※復元不可';
                        	}
                        	else
                        	{
                        		echo 'しない';
                        	}
                        	
                        	echo $this->Form->hidden('del_flg');
                        ?>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                    	<?php
							echo $this->Form->submit
							(
								'確定',
								array
								(
									'name' => 'submit-ok',
									'class' => 'btn btn-primary',
									'div' => FALSE
								)
							);
						?>
						
						<?php
							echo $this->Form->submit
							(
								'戻る',
								array
								(
									'name' => 'submit-back',
									'class' => 'btn btn-default',
									'div' => FALSE
								)
							);
                    	?>
                    </div>
                </div>
            </fieldset>
    	<?php
    		echo $this->Form->end();
    	?>
    </div>
</div>