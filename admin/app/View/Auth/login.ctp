<!DOCTYPE html>
<html>
  <head>
    <title>新着情報管理</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <?php echo $this->Html->css('/bootstrap/css/bootstrap.min'); ?>
    <!-- styles -->
    <?php echo $this->Html->css('styles'); ?>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="login-bg">
  	<div class="header">
	     <div class="container">
	        <div class="row">
	           <div class="col-md-12">
	              <!-- Logo -->
	              <div class="logo">
	                 <h1><a href="index.html">新着情報管理</a></h1>
	              </div>
	           </div>
	        </div>
	     </div>
	</div>

	<div class="page-content container">
		<div class="row">
			<div class="col-md-4 col-md-offset-4">
				<div class="login-wrapper">
			        <div class="box">
			            <div class="content-wrap">
                        	<?php
                        		echo $this->Form->create
                        		(
                        			'Admin',
                        			array
                        			(
                        				'type' => 'post',
                        				'url' => array
                        				(
	                        				'controller' => 'auth',
		                        			'action' => 'login'
	                        			)
                        			)
                        		);
                        	?>
                                <h6>ログイン</h6>
                                
                                <?php
                                	$flash = $this->Session->flash();
                                	
                                	if ($flash)
                                	{
                                ?>
                                
                                <div class="alert <?php echo $flashClass; ?>" role="alert">
                                	<strong>
 		                                <?php echo $flash; ?>
	                                </strong>
                                </div>
                                
                                <?php
                                	}
                                ?>
                                
                                
                                <?php
                                	echo $this->Form->input
                                	(
                                		'password',
                                		array
                                		(
                                			'type' => 'password',
                                			'class' => 'form-control',
                                			'div' => FALSE,
                                			'label' => FALSE,
                                			'placeholder' => 'パスワード'
                                		)
                                	);
                                ?>
                                
                                
                                <?php
                                	echo $this->Form->input
                                	(
                                		'username',
                                		array
                                		(
                                			'type' => 'hidden',
                                			'value' => 'admin1'
                                		)
                                	);
                                ?>
                                
                                
                                <div class="action">
                                    <input type="submit" class="btn btn-primary signup" value="ログイン">
                                </div>
                        	<?php
                        		echo $this->Form->end();
                        	?>
			            </div>
			        </div>
			    </div>
			</div>
		</div>
	</div>



    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://code.jquery.com/jquery.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <?php echo $this->Html->script('/bootstrap/js/bootstrap.min'); ?>
    <?php echo $this->Html->script('custom'); ?>
  </body>
</html>