	<div class="content-box-large">
		<div class="panel-heading">
		<div class="panel-title">
        	パスワード
        </div>
	</div>
		<div class="panel-body">
		
    	<?php
    		echo $this->Form->create
    		(
    			'Admin',
    			array
    			(
    				'type' => 'post',
        			'class' => 'form-horizontal',
        			'role' => 'form'
    			)
    		);
    	?>
        
            <fieldset>
            	
                <?php
                	$errorClass = '';
                	$errorMessage = '';
                	
                	
                	if (isset($validationErrors['password_old']))
                	{
                		$errorClass = ' has-error';
                		$errorMessage = implode('', $validationErrors['password_old']);
                	}
                ?>
            	
            	
                <div class="form-group <?php echo $errorClass; ?>">
                    <label class="col-sm-2 control-label">
                    	現在のパスワード
                    </label>
                    <div class="col-sm-10">
                        <?php
                        	echo $this->Form->input
                        	(
                        		'password_old',
                        		array
                        		(
                        			'type' => 'password',
                        			'class' => 'form-control',
                        			'div' => FALSE,
                        			'label' => FALSE
                        		)
                        	);
                        	
                        	
                        	if (!empty($errorMessage))
                        	{
                        		printf
                        		(
                        			'<span class="help-block"><i class="fa fa-warning"></i> %s</span>',
                        			$errorMessage
                        		);
                        	}
                        ?>
                    </div>
                </div>
                
                
                <?php
                	$errorClass = '';
                	$errorMessage = '';
                	
                	
                	if (isset($validationErrors['password']))
                	{
                		$errorClass = ' has-error';
                		$errorMessage = implode('', $validationErrors['password']);
                	}
                ?>
                
                
                <div class="form-group <?php echo $errorClass; ?>">
                    <label class="col-sm-2 control-label">
                    	新しいパスワード
                    </label>
                    <div class="col-sm-10">
                        <?php
                        	echo $this->Form->input
                        	(
                        		'password',
                        		array
                        		(
                        			'type' => 'password',
                        			'class' => 'form-control',
                        			'div' => FALSE,
                        			'label' => FALSE,
                        			'error' => FALSE
                        		)
                        	);
                        	
                        	
                        	if (!empty($errorMessage))
                        	{
                        		printf
                        		(
                        			'<span class="help-block"><i class="fa fa-warning"></i> %s</span>',
                        			$errorMessage
                        		);
                        	}
                        ?>
                    </div>
                </div>
                
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                      <button type="submit" class="btn btn-primary">変更</button>
                    </div>
                </div>
            </fieldset>
    	<?php
    		echo $this->Form->end();
    	?>
    </div>
</div>