<div class="dataTables_paginate paging_bootstrap">
    <ul class="pagination">
    
		<?php
			$paginatorParams = $this->Paginator->params();
			
			
			if ($paginatorParams['prevPage'])
			{
		?>
            <li class="prev">
            	<?php
            		echo $this->Html->link
            		(
            			'<i class="glyphicon glyphicon-fast-backward"></i>',
            			array
            			(
            				'page' => 1
            			),
            			array
            			(
            				'escape' => FALSE
            			)
            		);
            	?>
            </li>
            <li class="prev">
            	<?php
            		echo $this->Html->link
            		(
            			'<i class="glyphicon glyphicon-chevron-left"></i>',
            			array
            			(
            				'page' => $paginatorParams['page'] - 1
            			),
            			array
            			(
            				'escape' => FALSE
            			)
            		);
            	?>
            </li>
		<?php
			}
			else
			{
		?>
            <li class="prev disabled">
            	<a href="#">
            		<i class="glyphicon glyphicon-fast-backward"></i>
            	</a>
            </li>
            <li class="prev disabled">
                <a href="#">
                    <i class="glyphicon glyphicon-chevron-left"></i>
                </a>
            </li>
		<?php
			}
			
			
			
			
			
        	echo $this->Paginator->numbers
        	(
        		array
        		(
        			'separator' => NULL,
        			'tag' => 'li',
        			'currentClass' => 'active',
        			'currentTag' => 'a',
        			'escape' => FALSE
        		)
        	);
        	
        	
        	
        	
        	
			if ($paginatorParams['nextPage'])
			{
		?>
            <li class="next">
            	<?php
            		echo $this->Html->link
            		(
            			'<i class="glyphicon glyphicon-chevron-right"></i>',
            			array
            			(
            				'page' => $paginatorParams['page'] + 1
            			),
            			array
            			(
            				'escape' => FALSE
            			)
            		);
            	?>
            </li>
            <li class="next">
            	<?php
            		echo $this->Html->link
            		(
            			'<i class="glyphicon glyphicon-fast-forward"></i>',
            			array
            			(
            				'page' => $paginatorParams['pageCount']
            			),
            			array
            			(
            				'escape' => FALSE
            			)
            		);
            	?>
            </li>
		<?php
			}
			else
			{
		?>
            <li class="next disabled">
            	<a href="#">
            		<i class="glyphicon glyphicon-chevron-right"></i>
            	</a>
            </li>
            <li class="next disabled">
                <a href="#">
                    <i class="glyphicon glyphicon-fast-forward"></i>
                </a>
            </li>
		<?php
			}
		?>
        
        
    </ul>
</div>