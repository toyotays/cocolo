<?php
/*
	共通ヘルパー
	2015/01/03 nakayama
*/
App::uses('Helper', 'View');

class AppHelper extends Helper
{
	public $helpers = array('Html');
	
	
	public function d($data, $title = FALSE)
	{
		echo '<hr>';
		
		
		if ($title)
		{
			printf
			(
				'<h1>%s</h1>',
				$title
			);
		}
		
		
		printf
		(
			'<pre>%s</pre>',
			print_r($data, TRUE)
		);
	}
}
