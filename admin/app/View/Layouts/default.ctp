<!DOCTYPE html>
<html>
  <head>
    <title>新着情報管理</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- jQuery UI -->
    <link href="https://code.jquery.com/ui/1.10.3/themes/redmond/jquery-ui.css" rel="stylesheet" media="screen">

    <!-- Bootstrap -->
    <?php echo $this->Html->css('/bootstrap/css/bootstrap.min'); ?>
    <!-- styles -->
    <?php echo $this->Html->css('styles'); ?>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    
    
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://code.jquery.com/jquery.js"></script>
    <!-- jQuery UI -->
    <script src="https://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <?php echo $this->Html->script('/bootstrap/js/bootstrap.min'); ?>
    <?php echo $this->Html->script('custom'); ?>
    
    
	<?php echo $this->Html->script('tinymce/tinymce.min'); ?>
	<?php echo $this->Html->script('tinymce'); ?>
  </head>
  <body>
  	<div class="header">
	     <div class="container">
	        <div class="row">
	           <div class="col-md-12">
	              <!-- Logo -->
	              <div class="logo">
	                 <h1>
	                 	<?php
	                 		echo $this->Html->link
	                 		(
	                 			'新着情報管理',
	                 			array
	                 			(
	                 				'controller' => 'article',
	                 				'action' => 'index'
	                 			)
	                 		);
	                 	?>
	                 </h1>
	              </div>
	           </div>
	        </div>
	     </div>
	</div>

    <div class="page-content">
    	<div class="row">
		  <div class="col-md-2">
		  	<div class="sidebar content-box">
                <ul class="nav">
                    <!-- Main menu -->
                    
                    <?php
                    	$linkSets = array
                    	(
                    		array
                    		(
                    			'url' => array
                    			(
									'controller' => 'article',
	                    			'action' => 'index'
                    			),
                    			'icon' => 'glyphicon-list',
                    			'label' => '記事'
                    		),
                    		array
                    		(
                    			'url' => array
                    			(
	                    			'controller' => 'auth',
	                    			'action' => 'password',
                    			),
                    			'icon' => 'glyphicon-cog',
                    			'label' => 'パスワード'
                    		),
                    		array
                    		(
                    			'url' => array
                    			(
	                    			'controller' => 'auth',
	                    			'action' => 'logout'
                    			),
                    			'icon' => 'glyphicon-off',
                    			'label' => 'ログアウト'
                    		)
                    	);
                    	
                    	
                    	foreach ($linkSets as $linkSet)
                    	{
	                 		$url = $this->Html->url($linkSet['url']);
	                 		
	                 		if
	                 		(
	                 			$linkSet['url']['controller'] == $this->request->params['controller'] &&
	                 			$linkSet['url']['action'] == $this->request->params['action']
	                 		)
	                 		{
	                 			$class = 'current';
	                 		}
	                 		else
	                 		{
	                 			$class = '';
	                 		}
	                 		
	                 		?>
	                 		
		                    <li class="<?php echo $class; ?>">
		                    	<a href="<?php echo $url; ?>">
		                    		<i class="glyphicon <?php echo $linkSet['icon']; ?>"></i> <?php echo $linkSet['label']; ?>
		                    	</a>
		                    </li>
	                 		
	                 		<?php
                    	}
                    ?>
                </ul>
             </div>
		  </div>
		  <div class="col-md-10">
			
			<?php
				$flash = $this->Session->flash();
				
				if ($flash)
				{
			?>
				<div class="alert alert-success" role="alert">
					<strong>
				            <?php echo $flash; ?>
				    </strong>
				</div>
			<?php
				}
			?>

			<?php echo $this->fetch('content'); ?>

		  </div>


      <?php
      	echo $this->Html->css
      	(
      		'/vendors/datatables/dataTables.bootstrap',
      		'stylesheet',
      		array
      		(
      			'media' => 'screen'
      		)
      	);
      ?>
  </body>
</html>