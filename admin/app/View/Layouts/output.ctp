<?php
	$lines = $this->fetch('content');
	
	$linesArray = explode("\n", $lines);
	$linesArray = array_map('trim', $linesArray);
	
	
	foreach ($linesArray as $line)
	{
		if (empty($line))
		{
			continue;
		}
		
		
		$text = addslashes($line).'\r\n';
		
		
		printf
		(
			"document.write('%s');\r\n",
			$text
		);
	}
?>
