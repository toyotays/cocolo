	<dt>
        <h4>
			<?php
				echo htmlspecialchars($data['Article']['title']);
			?>
        </h4>
        <span>
			<?php
				echo date
				(
					'Y.m.d',
					$data['Article']['regist_dt']
				);
			?>
        </span>
    </dt>
    <dd>
		<?php
			echo $data['Article']['body'];
		?>
    </dd>