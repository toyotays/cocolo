tinymce.init
(
    {
        selector: "textarea.tinymce",
        language: "ja",
        plugins: "advlist hr searchreplace save contextmenu textcolor colorpicker jbimages table",
        toolbar1: "jbimages | undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent",
        toolbar2: "forecolor backcolor",
        relative_urls: false,
        height: 400
    }
 );