<?php
$footer = '
	<footer>
		<section class="NaviFooter">
			<ul class="NaviFooter-contact">
				<li class="sp-mr45"><a href="index.php"><img src="img/common/footer_logo.png" /></a></li>
				<li class="sp-mr25"><img src="img/common/footer_tel.png" alt="0798-31-7616" /></li>
				<li><a href="inquiry.php"><img src="img/common/footer_mail.png" /></a></li>
			</ul>
			<ul id="NaviFooter-navi">
				<li><a href="index.php">HOME</a></li>
				<li><a href="welcome.php">はじめての方へ</a></li>
				<li><a href="menu.php">メニュー</a></li>
				<li><a href="shop.php">店舗情報</a></li>
				<li><a href="owner.php">オーナー紹介</a></li>
				<li><a href="inquiry.php">お問い合わせ</a></li>
			</ul>
		</section>
		<div class="NaviFooter-copy"><p>Copyright©cocolo All right reserved.</p></div>
	</footer>
';
?>