<?php
include('layout.php');
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>インディバ・脱毛ができるエステサロンcocolo｜西宮　甲子園口</title>
<meta name="Keywords" content="インディバ,脱毛,エステ,西宮,甲子園口" />
<meta name="Description" content="cocoloはインディバを使ったプライベートエステサロンです。最新鋭の脱毛マシンも導入しておりますので、小学生のお子様でも安心安全です。" />
<link href="css/import.css" rel="stylesheet" type="text/css">

<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script><!--//back-to-->
<script type="text/javascript" src="js/multihero.js"></script><!--//back-to-->
<script type="text/javascript"> 
// pagetop
$(document).ready(function(){
	// hide #back-top first
	$("#back-top").hide();
	
	// fade in #back-top
	$(function () {
		$(window).scroll(function () {
			if ($(this).scrollTop() > 100) {
				$('#back-top').fadeIn();
			} else {
				$('#back-top').fadeOut();
			}
		});

		// scroll body to 0px on click
		$('#back-top a').click(function () {
			$('body,html').animate({
				scrollTop: 0
			}, 300);
			return false;
		});
	});

});
</script>
<!-- itsuaki script -->
<link rel="stylesheet" type="text/css" href="https://www.tsunagu-yoyaku.jp/src/css/skwindow.css">
<script type="text/javascript" src="https://www.tsunagu-yoyaku.jp/src/js/jquery-1.8.3.js"></script>
<script type="text/javascript" src="https://www.tsunagu-yoyaku.jp/src/js/jquery-ui-1.9.2.min.js"></script>
<script type="text/javascript" src="https://www.tsunagu-yoyaku.jp/src/js/common.js"></script>
<script type="text/javascript" src="https://www.tsunagu-yoyaku.jp/src/js/app_common.js"></script>
<script type="text/javascript" src="https://www.tsunagu-yoyaku.jp/src/js/skwindow_jq.js"></script>
<script>
$('#open_help_header').die().live("click", function() {
_app.popup.showIframe2('https://www.itsuaki.com/yoyaku/webreserve/staffsel?str_id=886&popflg=1',
{ noBlindClick: true, width: 1010, height: 600 });
});
</script>
<script>
$('#open_help_header2').die().live("click", function() {
_app.popup.showIframe2('https://www.itsuaki.com/yoyaku/webreserve/staffsel?str_id=886&popflg=1',
{ noBlindClick: true, width: 1010, height: 600 });
});
</script>
<script>
$('#open_help_header3').die().live("click", function() {
_app.popup.showIframe2('https://www.itsuaki.com/yoyaku/webreserve/staffsel?str_id=886&popflg=1',
{ noBlindClick: true, width: 1010, height: 600 });
});
</script>
<!-- // itsuaki script -->
</head>


<body>


	<?=$header?>
	<?=$navi?>


<section class="h3Sub">
	<h3><img src="img/menu01/h3.png" alt="インディバ"/></h3>
</section>


<article class="clear">



<section id="main">

	<section class="IntroMenu01">
    	<p>インディバはスペインで開発された痩身効果、美肌効果、体質改善、<br />
    	  美容業界でも最も注目されている、たるみの改善やサイズダウンに効果的です。<br />
    	  デトックスやホルモンバランスを整える働きもあり、体質改善、代謝UPにもつながります。<br />
    	  インディバ（INDIBA）とは、電気メスの発明者であるスペインの物理医学博士ホセ・<br />
    	  カルベット氏（インディバ社）により開発された、「高周波温熱機器」です。人体の各組織<br />
    	  にジュール熱を発生させます。インディバの特長は、身体の深部まで容量のエネルギー<br />
    	  を透過し、各組織に安全レベルのジュール熱を容易に発生させることができるので、体内組織の温暖化を実現させます。</p>
    </section>
	
	<dl class="Price01Menu">
    	<dt><img src="img/menu/pricettl01.png" alt="Price"/></dt>
      <div class="tax"><p>※消費税込</p></div>
        <dd>
        	<ul>
            	<li>
                	<dl>
                    	<dt><h5>インディバ　オーダーメイドフェイシャル</h5></dt>
                        <dd>
                        	<ul>
                            	<li><div class="Label03Menu">￥11,800</div><div class="Label02Menu">70min.</div></li>
                           	  <li><div class="Label03Menu">￥5,900</div><div class="Label02Menu">60min.</div><div class="Label01Menu">お試し</div></li>
                            </ul>
                        </dd>
                    </dl>
                </li>
                
            	<li>
                	<dl>
                    	<dt><h5>インディバ　オーダーメイドボディ</h5></dt>
                        <dd>
                        	<ul>
                            	<li>
                            	  <div class="Label03Menu">￥12,800</div><div class="Label02Menu">70min.</div></li>
                           	  <li>
                           	    <div class="Label03Menu">￥6,400</div><div class="Label02Menu">60min.</div><div class="Label01Menu">お試し</div></li>
                            </ul>
                        </dd>
                    </dl>
                </li>
                
            </ul>
        </dd>
    </dl>
    
    <dl class="Price01Menu">
      <div class="tax sp-pt50"><p>※消費税込</p></div>
        <dd>
        	<ul>
            	<li>
                	<dl>
                    	<dt><h5>インディバフェイシャル+インディバヘッド</h5>
                        <p>クレンジング→ふきとり→インディバフェイシャル→ふきとり→整肌+インディバヘッド</p>
						</dt>
                        <dd>
                        	<ul>
                            	<li><div class="Label03Menu">￥11,000</div><div class="Label02Menu">90min.</div></li>
                            </ul>
                        </dd>
                    </dl>
                </li>
                
            	<li>
                	<dl>
                    	<dt><h5>インディバヘッド</h5>
                        <p>岩盤マット付き</p>
						</dt>
                        <dd>
                        	<ul>
                            	<li><div class="Label03Menu">￥6,000</div><div class="Label02Menu">30min.</div></li>
                            </ul>
                        </dd>
                    </dl>
                </li>
                
            	<li>
                	<dl>
                    	<dt><h5>インディバボディ</h5>
                        <p>岩盤マット+インディバ+岩盤マット</p>
						</dt>
                        <dd>
                        	<ul>
                            	<li>
                            	  <p>気になる身体の部分１ヶ所セレクト。上半身全面or下半身全面or後面</p>
                                  <div class="Label03Menu">￥9,000</div><div class="Label02Menu">ボディ　70min.</div>
                                </li>
                                
                            	<li>
                            	  <p>気になる身体の部分２ヶ所セレクト。上半身全面or下半身全面or後面</p>
                                  <div class="Label03Menu">￥20,000</div><div class="Label02Menu">ボディ　150min.</div>
                                </li>
                                
                            	<li>
                            	  <p>気になる身体の部分２ヶ所セレクト。上半身全面or下半身全面or後面+肩甲背マッサージ</p>
                                  <div class="Label03Menu">￥30,000</div><div class="Label02Menu">ボディ　200min.</div>
                                </li>
                            </ul>
                        </dd>
                    </dl>
                </li>
                
            	<li>
                	<dl>
                    	<dt><h5>インディバフェイシャル</h5>
                        <p>クレンジング→ふきとり→インディバ→ふきとり→整肌</p>
						</dt>
                        <dd>
                        	<ul>
                            	<li>
                            	  <p>ベーシック美肌コース</p>
                                  <div class="Label03Menu">￥8,000</div><div class="Label02Menu">60min.</div>
                                </li>
                                
                            	<li>
                            	  <p>ベーシック+うるおい肌+リフトアップコース<br />
                           	      （高濃度ビタミンC導入）                            	  </p>
                                  <div class="Label03Menu">￥16,000</div><div class="Label02Menu">90min.</div>
                                </li>
                                
                            	<li>
                            	  <p>肌のお悩み一括改善コース<br />
                           	      （高濃度ビタミンCイオン導入・プラセンタイオン導入）                            	  </p>
                                  <div class="Label03Menu">￥20,000</div><div class="Label02Menu">120min.</div>
                                </li>
                            </ul>
                        </dd>
                    </dl>
                </li>
                
            	<li>
                	<dl>
                    	<dt><h5>セルキュアエステ</h5>
						</dt>
                        <dd>
                        	<ul>
                            	<li>美筋トレーニング（※高濃度ビタミンC導入）
                                  <div class="Label03Menu">￥6,000</div><div class="Label02Menu">40min.</div>
                                </li>
                                
                            	<li>
                                	＜OPTION（税込）＞<br />
                                  プラセンタ入 ローションパック　｜　￥500<br />
                                  コラーゲン入 ローションパック　｜　￥500<br />
                                  リズム炭酸ガスパック　｜　￥2,000
                                    
                              </li>
                                
                            </ul>
                        </dd>
                    </dl>
                </li>
                
                
            </ul>
        </dd>
    </dl>

    
    <dl class="ContactSub">
    	<dt><img src="img/common/contact_ttl.png" alt="ご予約・お問い合わせ"/></dt>
        <dd>
        	<ul>
            	<li><img src="img/common/contact_tel.png" alt="TEL：0798-31-7616"/></li>
                <li><a href="inquiry.php"><img src="img/common/contact_mail.png" alt="メールでのご予約"/></a></li>
            </ul>
        </dd>
    </dl>

  
  <div class="Ttl01">
  	<h5>Ticket</h5>
    <p>チケット</p>
  </div>
  
  <div class="tax sp-pt50"><p>※消費税込</p></div>
    <table width="100%" border="0" class="Table01 sp-mb60">
      <tbody>
      
      <!-- 01 -->
        <tr>
          <th colspan="3" scope="col">インディバボディ</th>
        </tr>
        <tr>
          <td rowspan="2" class="Label01 Label02"><div>70分<br />コース</div></td>
          <td>10回分のご契約で<span class="color-gold">10％OFF</span></td>
          <td class="Price">￥81,000<br /></td>
        </tr>
        <tr>
          <td class="Label02">15回分のご契約で<span class="color-gold">15％OFF</span></td>
          <td class="Price Label02">￥114,750</td>
        </tr>
        
        <tr>
          <td rowspan="2" class="Label01 Label02"><div>150分<br />
            コース</div></td>
          <td>10回分のご契約で<span class="color-gold">10％OFF</span></td>
          <td class="Price">￥180,000<br /></td>
        </tr>
        <tr>
          <td class="Label02">15回分のご契約で<span class="color-gold">15％OFF</span></td>
          <td class="Price Label02">￥255,000</td>
        </tr>
        
        <tr>
          <td rowspan="2" class="Label01 Label02"><div>200分<br />
            コース</div></td>
          <td>10回分のご契約で<span class="color-gold">10％OFF</span></td>
          <td class="Price">￥270,000<br /></td>
        </tr>
        <tr>
          <td class="Label02">15回分のご契約で<span class="color-gold">15％OFF</span></td>
          <td class="Price Label02">￥382,500</td>
        </tr>
      <!-- 01 -->
      
      <!-- 02 -->
        <tr>
          <th colspan="3" scope="col">インディバフェイシャル</th>
        </tr>
        <tr>
          <td rowspan="2" class="Label01 Label02"><div>60分<br />
            コース</div></td>
          <td>10回分のご契約で<span class="color-gold">10％OFF</span></td>
          <td class="Price">￥72,000</td>
        </tr>
        <tr>
          <td class="Label02">15回分のご契約で<span class="color-gold">15％OFF</span></td>
          <td class="Price Label02">￥102,000</td>
        </tr>
        
        <tr>
          <td rowspan="2" class="Label01 Label02"><div>90分<br />
            コース</div></td>
          <td>10回分のご契約で<span class="color-gold">10％OFF</span></td>
          <td class="Price">￥144,000<br /></td>
        </tr>
        <tr>
          <td class="Label02">15回分のご契約で<span class="color-gold">15％OFF</span></td>
          <td class="Price Label02">￥204,000</td>
        </tr>
        
        <tr>
          <td rowspan="2" class="Label01 Label02"><div>120分<br />
            コース</div></td>
          <td>10回分のご契約で<span class="color-gold">10％OFF</span></td>
          <td class="Price">￥180,000<br /></td>
        </tr>
        <tr>
          <td class="Label02">15回分のご契約で<span class="color-gold">15％OFF</span></td>
          <td class="Price Label02">￥255,000</td>
        </tr>
      <!-- 02 -->
      
      
      </tbody>
    </table>
    
    
        <dl class="MenuSub">
    	<dt><img src="img/common/menu_navi_ttl.png" alt="Menu"/></dt>
        <dd>
        	<ul>
            	<li><a href="menu01.php"><img src="img/common/menu_navi01.png" alt="インディバ"/></a></li>
            	<li><a href="menu03.php"><img src="img/common/menu_navi02.png" alt="無痛脱毛"/></a></li>
            	<li><a href="menu05.php"><img src="img/common/menu_navi03.png" alt="フォトエステ"/></a></li>
            	<li><a href="menu07.php"><img src="img/common/menu_navi04.png" alt="ブライダルエステ"/></a></li>
        	</ul>
        	<ul>
            	<li><a href="menu06.php"><img src="img/common/menu_navi06.png" alt="フォーカスディ"/></a></li>
        	</ul>
        </dd>
    </dl>

    
</section>



	<?=$side?>



</article>


  
	<?=$footer?>



</body>
</html>
