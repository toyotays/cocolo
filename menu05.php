<?php
include('layout.php');
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>インディバ・脱毛ができるエステサロンcocolo｜西宮　甲子園口</title>
<meta name="Keywords" content="インディバ,脱毛,エステ,西宮,甲子園口" />
<meta name="Description" content="cocoloはインディバを使ったプライベートエステサロンです。最新鋭の脱毛マシンも導入しておりますので、小学生のお子様でも安心安全です。" />
<link href="css/import.css" rel="stylesheet" type="text/css">

<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script><!--//back-to-->
<script type="text/javascript" src="js/multihero.js"></script><!--//back-to-->
<script type="text/javascript"> 
// pagetop
$(document).ready(function(){
	// hide #back-top first
	$("#back-top").hide();
	
	// fade in #back-top
	$(function () {
		$(window).scroll(function () {
			if ($(this).scrollTop() > 100) {
				$('#back-top').fadeIn();
			} else {
				$('#back-top').fadeOut();
			}
		});

		// scroll body to 0px on click
		$('#back-top a').click(function () {
			$('body,html').animate({
				scrollTop: 0
			}, 300);
			return false;
		});
	});

});
</script>
<!-- itsuaki script -->
<link rel="stylesheet" type="text/css" href="https://www.tsunagu-yoyaku.jp/src/css/skwindow.css">
<script type="text/javascript" src="https://www.tsunagu-yoyaku.jp/src/js/jquery-1.8.3.js"></script>
<script type="text/javascript" src="https://www.tsunagu-yoyaku.jp/src/js/jquery-ui-1.9.2.min.js"></script>
<script type="text/javascript" src="https://www.tsunagu-yoyaku.jp/src/js/common.js"></script>
<script type="text/javascript" src="https://www.tsunagu-yoyaku.jp/src/js/app_common.js"></script>
<script type="text/javascript" src="https://www.tsunagu-yoyaku.jp/src/js/skwindow_jq.js"></script>
<script>
$('#open_help_header').die().live("click", function() {
_app.popup.showIframe2('https://www.itsuaki.com/yoyaku/webreserve/staffsel?str_id=886&popflg=1',
{ noBlindClick: true, width: 1010, height: 600 });
});
</script>
<script>
$('#open_help_header2').die().live("click", function() {
_app.popup.showIframe2('https://www.itsuaki.com/yoyaku/webreserve/staffsel?str_id=886&popflg=1',
{ noBlindClick: true, width: 1010, height: 600 });
});
</script>
<script>
$('#open_help_header3').die().live("click", function() {
_app.popup.showIframe2('https://www.itsuaki.com/yoyaku/webreserve/staffsel?str_id=886&popflg=1',
{ noBlindClick: true, width: 1010, height: 600 });
});
</script>
<!-- // itsuaki script -->
</head>


<body>


	<?=$header?>
	<?=$navi?>


<section class="h3Sub">
	<h3><img src="img/menu05/h3.png" alt="フォトエステ"/></h3>
</section>


<article class="clear">



<section id="main">

	<section class="IntroMenu05">
    	<p>光を照射することにより、さまざまな肌悩みやお顔全体の<br />
    	  アンチエイジングを促進する冷却美容も兼ね備えたフェイシャル<br />
   	    トリートメントです。より効果的に肌深部へ光を届け、肌悩みの原因に直接働きかけることができ痛みや皮膚表面へのダメージが少ないのも特長です。</p>
    </section>
    
	<div class="ttl01-750"><h4>特徴</h4><span>features</span></div>

    <ul class="FeaturesMenu05">
    	<img src="img/menu05/img01.png" class="sp-mb20" />
    	<li class="sp-mr30">加齢や紫外線などにより、肌の機能が低下し、エラスチンやコラーゲンの働きが弱まると肌を支えられず、シワやたるみの原因になっています。</li>
        <li class="sp-mr30">光を照射することにより、皮膚の真皮層までエネルギーが到達し肌細胞に働きかけ<br />
        ます。</li>
    	<li>コラーゲンやエラスチンの生成が促進<br />
   	    され、ハリや弾力が出てシワやたるみが<br />
   	    改善されます。また、毛穴の開きやくすみ、シミなどお肌全体のコンディションを整え<br />
   	    ます。</li>
    </ul>
    
    	<img src="img/menu05/ba.png" class="sp-mb100" alt="目に見える！嬉しい効果"/>


	<dl class="Price01Menu">
    	<dt><img src="img/menu/pricettl01.png" alt="Price"/></dt>
        
      <div class="tax"><p>※消費税込</p></div>
      <dd>
        	<ul>
            	<li>
                	<dl>
                    	<h6 id="parts01"></h6>
                        <dt><h5>ショートコース　｜　30分</h5>
                          <p>クレンジング　→　拭き取り　→　フォト1クール　→<br/>
                            フェイスマスク</p>						
						</dt>
                        <dd>
                        	<ul>
                            	<li><div class="Label03Menu">￥2,160</div><div class="Label02Menu">￥6,480　→</div><div class="Label01Menu">初回</div></li>
                           		<li><div class="Label03Menu">￥24,300</div><div class="Label02Menu">￥29,160　→</div><div class="Label01Menu">5回</div></li>
                           		<li><div class="Label03Menu">￥43,200</div><div class="Label02Menu">￥51,840　→</div><div class="Label01Menu">10回</div></li>
                           		<li><div class="Label03Menu">￥56,700</div><div class="Label02Menu">￥68,040　→</div><div class="Label01Menu">15回</div></li>
                            </ul>
                        </dd>
                    </dl>
                </li>
                <li>
                	<dl>
                    	<h6 id="parts01"></h6>
                        <dt>
                          <h5>スタンダードコース　｜　50分</h5>
                          <p>クレンジング　→　拭き取り　→　フォト2クール　→<br/>
                            フェイスマスク　→　ローション　→　整顔</p>						
						</dt>
                        <dd>
                        	<ul>
                            	<li><div class="Label03Menu">￥4,320</div><div class="Label02Menu">￥10,800　→</div><div class="Label01Menu">初回</div></li>
                           		<li><div class="Label03Menu">￥38,880</div><div class="Label02Menu">￥48,600　→</div><div class="Label01Menu">5回</div></li>
                           		<li><div class="Label03Menu">￥69,120</div><div class="Label02Menu">￥86,400　→</div><div class="Label01Menu">10回</div></li>
                           		<li><div class="Label03Menu">￥90,720</div><div class="Label02Menu">￥113,400　→</div><div class="Label01Menu">15回</div></li>
                            </ul>
                        </dd>
                    </dl>
                </li>
            	<!--<li>
                	<dl>
                    	<h6 id="parts01"></h6>
                        <dt><h5>スタンダードコース　｜　60分</h5>
	                        <p>クレンジング　→　拭き取り　→　フォト1クール　→<br/>
	                        フェイスマスク</p>						
                        </dt>
                        <dd>
                        	<ul>
                            	<li><div class="Label03Menu">￥4,320</div><div class="Label02Menu">￥10,800　→</div><div class="Label01Menu">初回</div></li>
                           		<li><div class="Label03Menu">￥38,880</div><div class="Label02Menu">￥48,600　→</div><div class="Label01Menu">5回</div></li>
                           		<li><div class="Label03Menu">￥69,120</div><div class="Label02Menu">￥86,400　→</div><div class="Label01Menu">10回</div></li>
                           		<li><div class="Label03Menu">￥90,720</div><div class="Label02Menu">￥113,400　→</div><div class="Label01Menu">15回</div></li>
                            </ul>
                        </dd>
                    </dl>
                </li>-->
            </ul>
        </dd>
    </dl>
    
    <dl class="ContactSub">
   	  <dt><img src="img/common/contact_ttl.png" alt="ご予約・お問い合わせ"/></dt>
        <dd>
        	<ul>
            	<li><img src="img/common/contact_tel.png" alt="TEL：0798-31-7616"/></li>
                <li><a href="inquiry.php"><img src="img/common/contact_mail.png" alt="メールでのご予約"/></a></li>
            </ul>
        </dd>
    </dl>
    
    
        <dl class="MenuSub">
    	<dt><img src="img/common/menu_navi_ttl.png" alt="Menu"/></dt>
        <dd>
        	<ul>
            	<li><a href="menu01.php"><img src="img/common/menu_navi01.png" alt="インディバ"/></a></li>
            	<li><a href="menu03.php"><img src="img/common/menu_navi02.png" alt="無痛脱毛"/></a></li>
            	<li><a href="menu05.php"><img src="img/common/menu_navi03.png" alt="フォトエステ"/></a></li>
            	<li><a href="menu07.php"><img src="img/common/menu_navi04.png" alt="ブライダルエステ"/></a></li>
        	</ul>
        	<ul>
            	<li><a href="menu06.php"><img src="img/common/menu_navi06.png" alt="フォーカスディ"/></a></li>
        	</ul>
        </dd>
    </dl>

    
</section>



	<?=$side?>



</article>


  
	<?=$footer?>



</body>
</html>
