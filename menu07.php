<?php
include('layout.php');
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>インディバ・脱毛ができるエステサロンcocolo｜西宮　甲子園口</title>
<meta name="Keywords" content="インディバ,脱毛,エステ,西宮,甲子園口" />
<meta name="Description" content="cocoloはインディバを使ったプライベートエステサロンです。最新鋭の脱毛マシンも導入しておりますので、小学生のお子様でも安心安全です。" />
<link href="css/import.css" rel="stylesheet" type="text/css">

<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script><!--//back-to-->
<script type="text/javascript" src="js/multihero.js"></script><!--//back-to-->
<script type="text/javascript"> 
// pagetop
$(document).ready(function(){
	// hide #back-top first
	$("#back-top").hide();
	
	// fade in #back-top
	$(function () {
		$(window).scroll(function () {
			if ($(this).scrollTop() > 100) {
				$('#back-top').fadeIn();
			} else {
				$('#back-top').fadeOut();
			}
		});

		// scroll body to 0px on click
		$('#back-top a').click(function () {
			$('body,html').animate({
				scrollTop: 0
			}, 300);
			return false;
		});
	});

});
</script>
<!-- itsuaki script -->
<link rel="stylesheet" type="text/css" href="https://www.tsunagu-yoyaku.jp/src/css/skwindow.css">
<script type="text/javascript" src="https://www.tsunagu-yoyaku.jp/src/js/jquery-1.8.3.js"></script>
<script type="text/javascript" src="https://www.tsunagu-yoyaku.jp/src/js/jquery-ui-1.9.2.min.js"></script>
<script type="text/javascript" src="https://www.tsunagu-yoyaku.jp/src/js/common.js"></script>
<script type="text/javascript" src="https://www.tsunagu-yoyaku.jp/src/js/app_common.js"></script>
<script type="text/javascript" src="https://www.tsunagu-yoyaku.jp/src/js/skwindow_jq.js"></script>
<script>
$('#open_help_header').die().live("click", function() {
_app.popup.showIframe2('https://www.itsuaki.com/yoyaku/webreserve/staffsel?str_id=886&popflg=1',
{ noBlindClick: true, width: 1010, height: 600 });
});
</script>
<script>
$('#open_help_header2').die().live("click", function() {
_app.popup.showIframe2('https://www.itsuaki.com/yoyaku/webreserve/staffsel?str_id=886&popflg=1',
{ noBlindClick: true, width: 1010, height: 600 });
});
</script>
<script>
$('#open_help_header3').die().live("click", function() {
_app.popup.showIframe2('https://www.itsuaki.com/yoyaku/webreserve/staffsel?str_id=886&popflg=1',
{ noBlindClick: true, width: 1010, height: 600 });
});
</script>
<!-- // itsuaki script -->
</head>


<body>


	<?=$header?>
	<?=$navi?>


<section class="h3Sub">
	<h3><img src="img/menu07/h3.png" alt="ブライダルエステ"/></h3>
</section>


<article class="clear">



<section id="main">

	<section class="IntroMenu07">
    	<p>特別な一瞬が、一生の思い出になるように…<br />
    	  エステサロンCocoloでは、花嫁様への想いも込めて、確かな技術をもって<br />
    	  施術致します。ゆっくりと時間をかけて準備をされるプランや、短期での<br />
    	  結果が期待できるプラン、またお日取りやご予算に合わせたオリジナルの<br />
    	  プランもございます。<br />
    	</p>
    </section>
    

    
    <dl class="Price01Menu">
   	  <dt><img src="img/menu/pricettl01.png" alt="Price"/></dt>
        
      <div class="tax"><p>※消費税込</p></div>
      <dd>
        	<ul class="sp-mb100">
            	<li>
                	<dl>
                    	<h6 id="parts01"></h6>
                        <dt>
                          <h5>ブライダルコース　A　｜　120分コース</h5>
                          <p>岩盤浴マット 背中 ハミ肉 デコルテ首 肩 顔 の<br />
                            インディバ施術！

毎回リズムの炭酸ガスパック付き！ </p>						
						</dt>
                        <dd>
                        	<ul>
                            	<li><div class="Label03Menu">￥42,000</div><div class="Label01Menu">3回</div></li>
                            	<li><div class="Label03Menu">￥63,000</div><div class="Label01Menu">5回</div></li>
                            	<li><div class="Label03Menu">￥91,000</div><div class="Label01Menu">8回</div></li>
                            </ul>
                        </dd>
                    </dl>
                </li>
                <li>
                	<dl>
                    	<h6 id="parts01"></h6>
                        <dt><h5>ブライダルコースB　｜　200分コース</h5>
                          <p>A➕背中を中心とした全身インディバ施術<br />
							背中にも毎回リズムの炭酸ガスパック！
                            </p>						
						</dt>
                        <dd>
                        	<ul>
                            	<li><div class="Label03Menu">￥36,000</div><div class="Label01Menu">1回</div></li>
                            	<li><div class="Label03Menu">￥102,000</div><div class="Label01Menu">3回</div></li>
                            	<li><div class="Label03Menu">￥194,000</div><div class="Label01Menu">6回</div></li>
                            </ul>
                        </dd>
                    </dl>
                </li>
                </ul>
                
                
                
          <div class="Ttl01 sp-mb30">
            <h5>オリジナルブライダルプランコース</h5>
            <p>お客様のお悩みに応じて ブライダルプランをさせていただくコース</p>
          </div>
      <div class="tax"><p>※消費税込</p></div>
                <ul>
                <li>
                	<dl>
                    	<h6 id="parts01"></h6>
                        <dt>
                          <h5>温熱インディバボディケア　｜　上半身</h5>
                          <p>岩盤浴マット➡︎インディバ施術➡︎脂肪燃焼クリームでの<br />
                          ショートマッサージ</p>						
						</dt>
                        <dd>
                        	<ul>
                            	<li><div class="Label03Menu">￥19,440</div><div class="Label01Menu">120分</div></li>
                            </ul>
                        </dd>
                    </dl>
                </li>
                <li>
                	<dl>
                    	<h6 id="parts01"></h6>
                        <dt>
                          <h5>温熱インディバボディケア　｜　下半身<br />
                          （ヒップも含む）                          </h5>
                          <p>岩盤浴マット インディバ施術➡︎脂肪燃焼クリームでの<br />
                          ショートマッサージ</p>						
						</dt>
                        <dd>
                        	<ul>
                            	<li><div class="Label03Menu">￥19,440</div><div class="Label01Menu">120分</div></li>
                            </ul>
                        </dd>
                    </dl>
                </li>
                <li>
                	<dl>
                    	<h6 id="parts01"></h6>
                        <dt>
                          <h5>温熱インディバボディケア　｜　二の腕</h5>
						</dt>
                        <dd>
                        	<ul>
                            	<li>
                            	  <div class="Label03Menu">￥2,700</div>
                            	  <div class="Label01Menu">20分</div></li>
                            </ul>
                        </dd>
                    </dl>
                </li>
                <li>
                	<dl>
                    	<h6 id="parts01"></h6>
                        <dt>
                          <h5>温熱インディバボディケア　｜　<br />
                          肩甲骨マッサージ</h5>
						</dt>
                        <dd>
                        	<ul>
                            	<li>
                            	  <div class="Label03Menu">￥3,200</div>
                            	  <div class="Label01Menu">20分</div></li>
                            </ul>
                        </dd>
                    </dl>
                </li>
                <li>
                	<dl>
                    	<h6 id="parts01"></h6>
                        <dt>
                          <h5>全身たっぷり温熱インディバ</h5>
						</dt>
                        <dd>
                        	<ul>
                            	<li>
                            	  <div class="Label03Menu">￥34,200</div>
                            	  <div class="Label01Menu">180分</div></li>
                            </ul>
                        </dd>
                    </dl>
                </li>
                <li>
                	<dl>
                    	<h6 id="parts01"></h6>
                        <dt>
                          <h5>フェイシャルメニュー</h5>
						</dt>
                        <!--<dd>
                        	<ul>
                            	<li>
                            	  <div class="Label03Menu">￥34,200</div>
                            	  <div class="Label01Menu">180分</div></li>
                            </ul>
                        </dd>-->
                    </dl>
                </li>
            	<!--<li>
                	<dl>
                    	<h6 id="parts01"></h6>
                        <dt><h5>スタンダードコース　｜　60分</h5>
	                        <p>クレンジング　→　拭き取り　→　フォト1クール　→<br/>
	                        フェイスマスク</p>						
                        </dt>
                        <dd>
                        	<ul>
                            	<li><div class="Label03Menu">￥4,320</div><div class="Label02Menu">￥10,800　→</div><div class="Label01Menu">初回</div></li>
                           		<li><div class="Label03Menu">￥38,880</div><div class="Label02Menu">￥48,600　→</div><div class="Label01Menu">5回</div></li>
                           		<li><div class="Label03Menu">￥69,120</div><div class="Label02Menu">￥86,400　→</div><div class="Label01Menu">10回</div></li>
                           		<li><div class="Label03Menu">￥90,720</div><div class="Label02Menu">￥113,400　→</div><div class="Label01Menu">15回</div></li>
                            </ul>
                        </dd>
                    </dl>
                </li>-->
            </ul>
        </dd>
    </dl>
    
    <dl class="ContactSub">
   	  <dt><img src="img/common/contact_ttl.png" alt="ご予約・お問い合わせ"/></dt>
        <dd>
        	<ul>
            	<li><img src="img/common/contact_tel.png" alt="TEL：0798-31-7616"/></li>
                <li><a href="inquiry.php"><img src="img/common/contact_mail.png" alt="メールでのご予約"/></a></li>
            </ul>
        </dd>
    </dl>
    
    
        <dl class="MenuSub">
    	<dt><img src="img/common/menu_navi_ttl.png" alt="Menu"/></dt>
        <dd>
        	<ul>
            	<li><a href="menu01.php"><img src="img/common/menu_navi01.png" alt="インディバ"/></a></li>
            	<li><a href="menu03.php"><img src="img/common/menu_navi02.png" alt="無痛脱毛"/></a></li>
            	<li><a href="menu05.php"><img src="img/common/menu_navi03.png" alt="フォトエステ"/></a></li>
            	<li><a href="menu07.php"><img src="img/common/menu_navi04.png" alt="ブライダルエステ"/></a></li>
        	</ul>
        	<ul>
            	<li><a href="menu06.php"><img src="img/common/menu_navi06.png" alt="フォーカスディ"/></a></li>
        	</ul>
        </dd>
    </dl>

    
</section>



	<?=$side?>



</article>


  
	<?=$footer?>



</body>
</html>
