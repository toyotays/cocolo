<?php
include('layout.php');
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>インディバ・脱毛ができるエステサロンcocolo｜西宮　甲子園口</title>
<meta name="Keywords" content="インディバ,脱毛,エステ,西宮,甲子園口" />
<meta name="Description" content="cocoloはインディバを使ったプライベートエステサロンです。最新鋭の脱毛マシンも導入しておりますので、小学生のお子様でも安心安全です。" />
<link href="css/import.css" rel="stylesheet" type="text/css">

<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script><!--//back-to-->
<script type="text/javascript" src="js/multihero.js"></script><!--//back-to-->
<script type="text/javascript"> 
// pagetop
$(document).ready(function(){
	// hide #back-top first
	$("#back-top").hide();
	
	// fade in #back-top
	$(function () {
		$(window).scroll(function () {
			if ($(this).scrollTop() > 100) {
				$('#back-top').fadeIn();
			} else {
				$('#back-top').fadeOut();
			}
		});

		// scroll body to 0px on click
		$('#back-top a').click(function () {
			$('body,html').animate({
				scrollTop: 0
			}, 300);
			return false;
		});
	});

});
</script>


<!-- itsuaki script -->
<link rel="stylesheet" type="text/css" href="https://www.tsunagu-yoyaku.jp/src/css/skwindow.css">
<script type="text/javascript" src="https://www.tsunagu-yoyaku.jp/src/js/jquery-1.8.3.js"></script>
<script type="text/javascript" src="https://www.tsunagu-yoyaku.jp/src/js/jquery-ui-1.9.2.min.js"></script>
<script type="text/javascript" src="https://www.tsunagu-yoyaku.jp/src/js/common.js"></script>
<script type="text/javascript" src="https://www.tsunagu-yoyaku.jp/src/js/app_common.js"></script>
<script type="text/javascript" src="https://www.tsunagu-yoyaku.jp/src/js/skwindow_jq.js"></script>
<script>
$('#open_help_header').die().live("click", function() {
_app.popup.showIframe2('https://www.itsuaki.com/yoyaku/webreserve/staffsel?str_id=886&popflg=1',
{ noBlindClick: true, width: 1010, height: 600 });
});
</script>
<script>
$('#open_help_header2').die().live("click", function() {
_app.popup.showIframe2('https://www.itsuaki.com/yoyaku/webreserve/staffsel?str_id=886&popflg=1',
{ noBlindClick: true, width: 1010, height: 600 });
});
</script>
<script>
$('#open_help_header3').die().live("click", function() {
_app.popup.showIframe2('https://www.itsuaki.com/yoyaku/webreserve/staffsel?str_id=886&popflg=1',
{ noBlindClick: true, width: 1010, height: 600 });
});
</script>
<!-- // itsuaki script -->
</head>



<body>


	<?=$header?>
	<?=$navi?>


<section class="h3Sub">
	<h3><img src="img/shop/h3.png" alt="店舗情報"/></h3>
</section>


<article class="clear">



<section id="main">
	
    <dl class="Shop">
    	<dt><h4><img src="img/shop/h4.png" alt="完全個室のゆったりとした空間でそこはあなただけのプライベート空間" /></h4></dt>
        <dd>
        	<ul>
            	<li><h5>サロン名</h5><p>Cocolo（ココロ）</p></li>
            	<li><h5>住所</h5><p>兵庫県西宮市甲子園口北町19-16 2F</p></li>
            	<li><h5>TEL</h5><p>0798-31-7616</p></li>
            	<li><h5>営業時間</h5>
            	  <p> 月・水・金・土　：　１０時～２０時　　｜　　火・日　：　１０時～１７時</p>
            	</li>
            	<li><h5>定休日</h5><p>木曜日</p>
            	</li>
            	<li><h5>アクセス</h5><p>JR神戸線　甲子園口駅、北改札を出て徒歩２分</p></li>
            </ul>
        </dd>
        <dd>
        	<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3278.572498786768!2d135.3748785!3d34.74116869999999!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6000f1f80409bb07%3A0xde1919a3ee5e839!2z44CSNjYzLTgxMTIg5YW15bqr55yM6KW_5a6u5biC55Sy5a2Q5ZyS5Y-j5YyX55S677yR77yZ4oiS77yR77yW!5e0!3m2!1sja!2sjp!4v1421996726332" width="750" height="270" frameborder="0" style="border:0"></iframe>
        </dd>
    </dl>
    <img src="img/shop/img01.png" class="sp-mb70" />
    
    <dl class="ContactSub">
    	<dt><img src="img/common/contact_ttl.png" alt="ご予約・お問い合わせ"/></dt>
        <dd>
        	<ul>
            	<li><img src="img/common/contact_tel.png" alt="TEL：0798-31-7616"/></li>
                <li><a href="inquiry.php"><img src="img/common/contact_mail.png" alt="メールでのご予約"/></a></li>
            </ul>
        </dd>
    </dl>
    
    
        <dl class="MenuSub">
    	<dt><img src="img/common/menu_navi_ttl.png" alt="Menu"/></dt>
        <dd>
        	<ul>
            	<li><a href="menu01.php"><img src="img/common/menu_navi01.png" alt="インディバ"/></a></li>
            	<li><a href="menu03.php"><img src="img/common/menu_navi02.png" alt="無痛脱毛"/></a></li>
            	<li><a href="menu05.php"><img src="img/common/menu_navi03.png" alt="フォトエステ"/></a></li>
            	<li><a href="menu07.php"><img src="img/common/menu_navi04.png" alt="ブライダルエステ"/></a></li>
        	</ul>
        	<ul>
            	<li><a href="menu06.php"><img src="img/common/menu_navi06.png" alt="フォーカスディ"/></a></li>
        	</ul>
        </dd>
    </dl>

    
</section>



	<?=$side?>



</article>


  
	<?=$footer?>



</body>
</html>
