<?php
$side = '
	<section id="side">
	
		<dl class="EstheSide">
			<dt><img src="img/side/01_ttl.png" /></dt>
			<dd>
				<ul>
					<li><a href="menu01.php"><img src="img/side/01_navi01.png" alt="インディバ" /></a></li>
					<li><a href="menu03.php"><img src="img/side/01_navi03.png" alt="脱毛" /></a></li>
					<li><a href="menu05.php"><img src="img/side/01_navi05.png" alt="フォトエステ" /></a></li>
					<li><a href="menu07.php"><img src="img/side/01_navi06.png" alt="ブライダルエステ" /></a></li>
					<li><a href="menu06.php"><img src="img/side/01_navi07.png" alt="フォーカスD" /></a></li>
				</ul>
			</dd>
		</dl>
		
		<ul class="BnrSide">
			<li><a href="welcome.php"><img src="img/side/02_bnr01.png" alt="はじめての方へ" /></a></li>
			<li><a href="owner.php"><img src="img/side/02_bnr02.png" alt="オーナーエステティシャン　Nami" /></a></li>
			<li><a href="http://ameblo.jp/indiba-cocolo/" target="_blank"><img src="img/side/02_bnr03.png" alt="ブログ" /></a></li>
			<li><img src="img/side/02_bnr04.png" alt="クレジットカード、使えます。" /></li>
		</ul>
		
		<dl class="AddSide">
			<dt><img src="img/side/03_ttl.png" alt="ご予約・お問い合わせ" /></dt>
			<dd>
				<ul>
					<li class="sp-mb15"><img src="img/side/03_tel.png" alt="TEL：0798-31-7616" /></li>
					<li><a href="inquiry.php"><img src="img/side/03_mail.png" alt="メールでのご予約" /></a></li>
				</ul>
			</dd>
		</dl>
		
		<dl class="LinksSide">
			<dt><img src="img/side/04_ttl.png" alt="Links" /></dt>
			<dd>
				<ul>
					<li><a href="http://www.yuki-shinkyu.com/" target="_blank"><img src="img/side/04_bnr01.png" alt="ゆきしんきゅういん" /></a></li>
					<li><a href="http://beauty.hotpepper.jp/kr/slnH000321502/" target="_blank"><img src="img/side/04_bnr02.png" alt="ホットペッパービューティー"/></a></li>
				</ul>
			</dd>
		</dl>
		
	</section>

';
?>