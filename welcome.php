<?php
include('layout.php');
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>インディバ・脱毛ができるエステサロンcocolo｜西宮　甲子園口</title>
<meta name="Keywords" content="インディバ,脱毛,エステ,西宮,甲子園口" />
<meta name="Description" content="cocoloはインディバを使ったプライベートエステサロンです。最新鋭の脱毛マシンも導入しておりますので、小学生のお子様でも安心安全です。" />
<link href="css/import.css" rel="stylesheet" type="text/css">

<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script><!--//back-to-->
<script type="text/javascript" src="js/multihero.js"></script><!--//back-to-->
<script type="text/javascript"> 
// pagetop
$(document).ready(function(){
	// hide #back-top first
	$("#back-top").hide();
	
	// fade in #back-top
	$(function () {
		$(window).scroll(function () {
			if ($(this).scrollTop() > 100) {
				$('#back-top').fadeIn();
			} else {
				$('#back-top').fadeOut();
			}
		});

		// scroll body to 0px on click
		$('#back-top a').click(function () {
			$('body,html').animate({
				scrollTop: 0
			}, 300);
			return false;
		});
	});

});
</script>


<!-- itsuaki script -->
<link rel="stylesheet" type="text/css" href="https://www.tsunagu-yoyaku.jp/src/css/skwindow.css">
<script type="text/javascript" src="https://www.tsunagu-yoyaku.jp/src/js/jquery-1.8.3.js"></script>
<script type="text/javascript" src="https://www.tsunagu-yoyaku.jp/src/js/jquery-ui-1.9.2.min.js"></script>
<script type="text/javascript" src="https://www.tsunagu-yoyaku.jp/src/js/common.js"></script>
<script type="text/javascript" src="https://www.tsunagu-yoyaku.jp/src/js/app_common.js"></script>
<script type="text/javascript" src="https://www.tsunagu-yoyaku.jp/src/js/skwindow_jq.js"></script>
<script>
$('#open_help_header').die().live("click", function() {
_app.popup.showIframe2('https://www.itsuaki.com/yoyaku/webreserve/staffsel?str_id=886&popflg=1',
{ noBlindClick: true, width: 1010, height: 600 });
});
</script>
<script>
$('#open_help_header2').die().live("click", function() {
_app.popup.showIframe2('https://www.itsuaki.com/yoyaku/webreserve/staffsel?str_id=886&popflg=1',
{ noBlindClick: true, width: 1010, height: 600 });
});
</script>
<script>
$('#open_help_header3').die().live("click", function() {
_app.popup.showIframe2('https://www.itsuaki.com/yoyaku/webreserve/staffsel?str_id=886&popflg=1',
{ noBlindClick: true, width: 1010, height: 600 });
});
</script>
<!-- // itsuaki script -->
</head>



<body>


	<?=$header?>
	<?=$navi?>


<section class="h3Sub">
	<h3><img src="img/welcome/h3.png" alt="はじめての方へ"/></h3>
</section>


<article class="clear">



<section id="main">
	
    <section class="IntroWelcome">
    	<p>本日は当ホームページにご訪問いただき誠にありがとうございます。<br />
    	  Cocloでは、効果にこだわった施術をご提供致しております。<br />
    	  インディバなど美容機器を導入し、<br />
    	  オーナーエステティシャンNamiがカウンセリングさせていただき<br />
   	    お客様一人ひとりのお悩みに合った施術内容をお選びします。</p>
        
        
        
        <dl class="MenuSub">
    	<dt><img src="img/common/menu_navi_ttl.png" alt="Menu"/></dt>
        <dd>
        	<ul>
            	<li><a href="menu01.php"><img src="img/common/menu_navi01.png" alt="インディバ"/></a></li>
            	<li><a href="menu03.php"><img src="img/common/menu_navi02.png" alt="無痛脱毛"/></a></li>
            	<li><a href="menu05.php"><img src="img/common/menu_navi03.png" alt="フォトエステ"/></a></li>
            	<li><a href="menu07.php"><img src="img/common/menu_navi04.png" alt="ブライダルエステ"/></a></li>
        	</ul>
        	<ul>
            	<li><a href="menu06.php"><img src="img/common/menu_navi06.png" alt="フォーカスディ"/></a></li>
        	</ul>
        </dd>
    </dl>

    </section>
    
    
    
    <section class="AboutWelcome">
    	<h4><img src="img/welcome/about_ttl.png" alt="cocoloについて" /></h4>
        <p>誰でも気軽にご来店いただけるアットホームなエステサロンです。<br />
            お客様と一緒に様々なイベントも催しております。</p>
        <dl>
        	<dt><h5><img src="img/welcome/about_ttl02.png" alt="こんなイベントしています！" /></h5></dt>
            <dd>
            	<h6><img src="img/welcome/about_ttl_mini01.png" alt="酵素ジュース教室" /></h6>
                <p>お客様と一緒に酵素ジュース作りをしました。<br />
                体にとてもいいんですよ。</p>
            </dd>
            <dd class="sp-ml50">
            	<h6><img src="img/welcome/about_ttl_mini02.png" alt="ポーセラーツ教室" /></h6>
                <p>先生を招いて白いお皿にお好みの色や柄の転写紙を貼ったり、<br />
                  絵の具で着色したり絵を描いたりとオリジナルなお皿を作りました。</p>
            </dd>
        </dl>
        <div><p>他にもフラワーアレンジメント教室等様々なイベントを今後開催していきます。　お気軽にご参加くださいね。</p></div>
    </section>
    
    
    
	<div class="ttl01-750"><h4>店舗情報</h4><span>hop information</span></div>
    <ul class="InfoWelcome">
    	<div>
        	<li class="Label01Welcome"><h6>営業時間</h6><p>10：00～20：00（最終受付）</p></li>
        	<li class="Label02Welcome"><h6>定休日</h6><p>火曜日</p></li>
        	<li class="Label03Welcome"><h6>住所</h6><p>兵庫県西宮市甲子園口北町19-16 2F</p></li>
    	</div>
    	<div>
        	<li class="Label04Welcome"><h6>ご予約</h6><p>お電話でのご予約　090-6209-7190</p>
				<a href="inqury.php"><img src="img/welcome/info_mail.png" class="sp-ml120 sp-mt10"/></a>
            </li>
        	<li class="Label05Welcome"><h6>アクセス</h6><p>JR神戸線　甲子園口を降りて徒歩２分</p></li>
    	</div>
    </ul>
    <p>車でお越しの方は有料コインパーキングをご利用下さいますようお願い致します。</p>
    <p class="sp-mb30">
    	<span class="sp-pr50">コインパーキング　　<a href="http://www.its-mo.com/c/%EF%BC%AA%EF%BC%B2%E7%94%B2%E5%AD%90%E5%9C%92%E5%8F%A3%E5%8C%97%E7%AC%AC%EF%BC%92%E9%A7%90%E8%BC%AA%E5%A0%B4/DIDX_DKE%2C4350669/access/" target="_blank">①</a></span>
        <span class="sp-pr50"><a href="http://www.its-mo.com/c/%E3%83%91%E3%83%BC%E3%82%AFNET%E7%94%B2%E5%AD%90%E5%9C%92%E5%8F%A3%E9%A7%85%E5%89%8D%E9%A7%90%E8%BB%8A%E5%A0%B4/PPK_ZCOIN%2C00000000000002800923/access/" target="_blank">②</a></span>
        <span class="sp-pr50"><a href="http://www.its-mo.com/c/P%EF%BC%8EZONE%E7%94%B2%E5%AD%90%E5%9C%92%E5%8F%A3%E6%9D%B1/PPK_ZCOIN%2C00000000000002645670/access/" target="_blank">③</a></span></p>
   
   
    <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3278.572498786768!2d135.3748785!3d34.74116869999999!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6000f1f80409bb07%3A0xde1919a3ee5e839!2z44CSNjYzLTgxMTIg5YW15bqr55yM6KW_5a6u5biC55Sy5a2Q5ZyS5Y-j5YyX55S677yR77yZ4oiS77yR77yW!5e0!3m2!1sja!2sjp!4v1422324901279" width="750" height="250" frameborder="0" style="border:0;"></iframe>   
   
   
    
</section>



	<?=$side?>



</article>


  
	<?=$footer?>



</body>
</html>
